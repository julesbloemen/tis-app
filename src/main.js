import Vue from 'vue';

Vue.use(require('vue-moment'));
require('moment/locale/nl');

import Timecard from './tis-shared/components/Timecard.vue';
import Home from './components/Home.vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

// import 'onsenui/css/onsenui.css';
// import 'onsenui/css/onsen-css-components.css';

import './tis-shared/css/modern.css';
import './tis-shared/css/shared.css';
import './tis-shared/css/rangecalendar/rangecalendar.css';



Vue.component('ts-calendar', require('./tis-shared/components/Calendar.vue'));

/* eslint-disable no-new */
var Phonegap = {

    // Application Constructor
    initialize() {

        this.bindEvents();
    },


    bindEvents() {

        document.addEventListener('deviceready', this.onDeviceReady, false)
    },

    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'Phonegap.receivedEvent(...);'
    onDeviceReady() {
        Phonegap.receivedEvent();
    },

    // Update DOM on a Received Event
    receivedEvent() {

        // 0. If using a module system, call Vue.use(VueRouter)
        Vue.use(VueRouter);
        Vue.use(VueResource);

        // const routes = [
        //     {
        //         path: '/',
        //         name: 'home',
        //         component: Home,
        //     },
        //     // {
        //     //     path: '/mijnuren/*',
        //     //     name: 'Timecard page',
        //     //     component: Timecard,
        //     // }
        // ]


        var router = new VueRouter({
            mode: 'history',
            // base: window.location.href,
            routes: [
                {
                    path: '/',
                    name: 'home',
                    component: Home,
                },
                {
                    path: '/mijnuren/*',
                    name: 'Timecard page',
                    component: Timecard,
                }

            ]
        });

        const app = new Vue({
            router,
            data() {
                return {
                    msg: 'War is testt',
                    // windowWidth: 0,
                    // windowHeight: 0,
                    // isXs: false,
                    // isSm: false,
                    // isMd: false,
                    // isLg: false,
                    // isXl: false,
                    // xs: {max: 576},
                    // sm: {min: 577,
                    //     max: 767},
                    // md: {min: 768,
                    //     max: 991},
                    // lg: {min: 992,
                    //     max: 1199},
                    // xl: {min: 1200},
                    auth: false,
                    user: {}
                }
            },
            props: {
                logged: false, // assuming that your prop is an object
            },
            mounted: function () {
                this.$nextTick(function () {
                    // window.addEventListener('resize', this.getWindowWidth);
                    // window.addEventListener('resize', this.getWindowHeight);
                    // this.getWindowWidth();
                    // this.getWindowHeight();
                    this.$root.auth = false;
                    router.push('/');
                })
            },
            methods: {
                // getWindowWidth(event) {
                //     this.windowWidth = document.documentElement.clientWidth;
                //     this.isXs = this.windowWidth <= this.xs.max;
                //     this.isSm = this.windowWidth >= this.sm.min && this.windowWidth <= this.sm.max;
                //     this.isMd = this.windowWidth >= this.md.min && this.windowWidth <= this.md.max;
                //     this.isLg = this.windowWidth >= this.lg.min && this.windowWidth <= this.lg.max;
                //     this.isXl = this.windowWidth >= this.xl.min;
                //
                //     if (this.isXs) console.log('isXs');
                //     if (this.isSm) console.log('isSm');
                //     if (this.isMd) console.log('isMd');
                //     if (this.isLg) console.log('isLg');
                //     if (this.isXl) console.log('isXl');
                // },
                //
                // getWindowHeight(event) {
                //     this.windowHeight = document.documentElement.clientHeight;
                // },

            }
        }).$mount('#app')

        // const app = new Vue({
        //     router,
        //     data() {
        //         return {
        //             msg: 'War is mijn route?',
        //             windowWidth: 0,
        //             windowHeight: 0,
        //             isXs: false,
        //             isSm: false,
        //             isMd: false,
        //             isLg: false,
        //             isXl: false,
        //             xs: {max: 576},
        //             sm: {min: 577,
        //                 max: 767},
        //             md: {min: 768,
        //                 max: 991},
        //             lg: {min: 992,
        //                 max: 1199},
        //             xl: {min: 1200},
        //             auth: false,
        //             user: {}
        //         }
        //     },
        //     props: {
        //         logged: false, // assuming that your prop is an object
        //     },
        //     mounted: function () {
        //         this.$nextTick(function () {
        //             // window.addEventListener('resize', this.getWindowWidth);
        //             // window.addEventListener('resize', this.getWindowHeight);
        //             // this.getWindowWidth();
        //             // this.getWindowHeight();
        //             // this.$root.auth = false;
        //             console.log('conf');
        //         })
        //     },
        //     methods: {
        //         checkAuth: function() {
        //             console.log('checking cookie')
        //
        //             localStorage.setItem('user_id', 1);
        //             console.log("LOAD TOKENID" + localStorage.getItem('id_token'));
        //         },
        //         getWindowWidth(event) {
        //             this.windowWidth = document.documentElement.clientWidth;
        //             this.isXs = this.windowWidth <= this.xs.max;
        //             this.isSm = this.windowWidth >= this.sm.min && this.windowWidth <= this.sm.max;
        //             this.isMd = this.windowWidth >= this.md.min && this.windowWidth <= this.md.max;
        //             this.isLg = this.windowWidth >= this.lg.min && this.windowWidth <= this.lg.max;
        //             this.isXl = this.windowWidth >= this.xl.min;
        //
        //             if (this.isXs) console.log('isXs');
        //             if (this.isSm) console.log('isSm');
        //             if (this.isMd) console.log('isMd');
        //             if (this.isLg) console.log('isLg');
        //             if (this.isXl) console.log('isXl');
        //         },
        //
        //         getWindowHeight(event) {
        //             this.windowHeight = document.documentElement.clientHeight;
        //         },
        //
        //
        //     }
        // }).$mount('#app')

        // Now the app has started!
    }
}

Phonegap.initialize()
